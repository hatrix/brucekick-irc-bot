﻿from pypeul import *
import time
import re
import random

class SimpleBot(IRC):
    def on_ready(self):
        self.join("#epita-bro")

    def on_self_join(self, target):
        self.message(target, "Bonjour, Bruce_.")

    def on_channel_message(self, umask, target, msg):    
        badwords = ["rigolol", "pute", "connard", "pauvre", "kabyle",
                    "je( ne)? suis pas arabe", "\bcc", ":DD[D]+", 
                    "niquez? (ta|vos) mères?", "bof", "putain"]
                    
                    
        nicks = ["Bruce", "Bruce_", "Bruce__"]
        
        tg = 1
        
        if str(umask) in nicks:
            for word in badwords:
                if re.search(word, msg):
                    self.kick(target, umask, 'badword Bruce !')
                    tg = 0
                    break
            
            if tg:
                if random.randrange(120) == 5:
                    self.message(target, "tg Bruce_")
                else:
                    if re.search("BruceKick", msg):
                        n = random.randrange(5)
                        if n == 0:
                            self.message(target, "Ne me parle pas Bruce_")
                        elif n == 1:
                            self.message(target, 
                                        "Et si tu bossais au lieu de me hl ?")
                        elif n == 2:
                            self.message(target, 
                                        "Toujours en rattrapages Bruce_ ?")
                        elif n == 3:
                            self.message(target, 
                                "T'es vraiment sûr que t'as pas de boulot ?")
                        elif n == 4:
                            self.message(target, 
                                "Si déjà tu me parles, paye moi un kebab")


    def on_ctcp_version_request(self, umask, value):
        self.ctcp_reply(umask.nick, 'VERSION', "SimpleBot, powered by pypeul")

    def on_disconnected(self):
        logger.info('Disconnected. Trying to reconnect...')
        while True:
            try:
                self.connect('irc.rezosup.org', 6667)
                self.ident('BruceKick')
                self.run()
                break
            except:
                logger.error('Attempt failed. Retrying in 30s...')
            time.sleep(30)


if __name__ == '__main__':
    import logging
    logging.basicConfig(level=logging.DEBUG)

    bot = SimpleBot()
    bot.connect('irc.rezosup.org', 6667)
    bot.ident('BruceKick')
    bot.run()
